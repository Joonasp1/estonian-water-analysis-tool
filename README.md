# Estonian Water Analysis Tool

http://eestivesi.cloud.ut.ee

This project is for landowners, researchers and property designers who need a way to perform water drainage analysis on a particular land. The EWAT system is a weather and geographical data analysis tool that allows everybody to obtain information about how much water flows and accumulates in a particular land and if there is a risk of excessive water accumulation. In addition to that the system also visualizes general weather data as interactive graphs and does some predictions based on that data for scientific research purposes. Unlike existing prediction models, the EWAT system makes it possible to make predictions about a specific land and therefore is much more useful to landowners and property designers who have to make decisions considering that particular piece of land. Our product enables the future cities to solve excessive water accumulation problem by analysing the existing data and providing information about each individual land. This helps the decision makers to easily identify possible problem areas and take actions based on the water data provided by the EWAT system.

## Building instructions

Tested on Ubuntu 20.04 LTS.

### Step 1 - Install dependencies

```
sudo apt update
sudo apt install postgis nodejs npm git make curl
# free port 5432
sudo systemctl disable postgresql --now
```

### Step 2 - Install Docker
```
curl -fsSL https://get.docker.com -o get-docker.sh
chmod +x get-docker.sh
./get-docker.sh

sudo apt install docker-compose

sudo groupadd docker
sudo usermod -aG docker $USER
sudo reboot
```
### Step 3 - Clone the repository
```
git clone https://gitlab.com/Joonasp1/estonian-water-analysis-tool.git
cd estonian-water-analysis-tool
```

### Step 4 - Install node modules for the frontend
```
cd frontend
npm install
cd ..
```

### Step 5 - Build and run the Docker containers
```
make build
make deploy
```

### Step 6 - Add the data to the database (only do this once - on the first install)
```
cd database
./readShapeFiles.sh
./create_weatherdb_tables.sh
cd ..
make down
make deploy
```
### Step 7 - Go to localhost in a browser and verify that the application is working
Note: It might take a few minutes for the geoserver to start.

To stop the application run:
```
make down
```
To start it again run:
```
make deploy
```

## Deployment in production

Follow the same instructions but instead of step 4 and 5 run:
```
make deploy_production
```

