from flask import Blueprint, current_app
from flask import request
from sqlalchemy.sql.elements import outparam

from core.db import db_weather_session
from ewat_weather.models import Weather
from . import data_query


from datetime import date, timedelta

bp = Blueprint("ewat_weather", __name__)


def getDatesInRange(startDay, startMonth, startYear, endDay, endMonth, endYear):
    startDate = date(int(startYear), int(startMonth), int(startDay)) 
    endDate = date(int(endYear), int(endMonth), int(endDay)) 
    delta = endDate - startDate
    days = []
    for i in range(delta.days + 1):
        day = startDate + timedelta(days=i)
        days.append(day)
    return days # date objects


@bp.route("/hello")
def test():
    current_app.logger.info("Weather has been summoned.")
    return "Hello Weather!"


@bp.route("/params", methods=["GET"])
def weather_params():
    # get the names of different weather related params
    station_id = request.args.get("station_id")

    if not station_id:
        return {'status' : 'fail', "error:": "Please provide station_id as a get parameter."}
    
    try:
        result = db_weather_session.query(
            Weather.datadesc
        ).filter(Weather.station_id==station_id).distinct()

        output = {}
        data = []
        if result:
            output['status'] = 'success'
            output['station_id'] = station_id
            
            for row in result:
                data.append(row['datadesc'])
            output['params'] = data

    except Exception as e:
        output['status'] = 'fail'
        output['error'] = str(e)
        current_app.logger.error(e)
    
    return output

@bp.route("/history", methods=["GET"])
def history():
    # get historic weather info
    station_id = request.args.get("station_id")
    parameter = request.args.get("parameter")
    start_date = request.args.get("start")
    end_date = request.args.get("end")

    if not station_id:
        return {'status' : 'fail', "error:": "Please provide station_id as a get parameter."}
    
    try:
        query = db_weather_session.query(
            Weather.station_id,
            Weather.datadesc,
            Weather.content,
            Weather.creation_timestamp
        )

        # add optional filters
        filters = []
        filters.append(Weather.station_id == station_id)
        if parameter:
            filters.append(Weather.datadesc == parameter)
        
        if start_date:
            filters.append(Weather.creation_timestamp >= start_date)
        
        if end_date:
            filters.append(Weather.creation_timestamp <= end_date)
        
        result = query.filter(*filters).all()

        output = {}
        data = []
        if result:
            output['status'] = 'success'
            output['station_id'] = station_id
            
            for row in result:
                data.append({
                    'datadesc' : row['datadesc'],
                    'content' : row['content'],
                    'creation_timestamp' : row['creation_timestamp']
                })
            output['data'] = data

    except Exception as e:
        output['status'] = 'fail'
        output['error'] = str(e)
        current_app.logger.error(e)
    
    return output


@bp.route("/current")
def current_weather():
    # get current weather info
    stationName = request.args.get("station_name")
    if not stationName:
        return {"msg:": "Please provide station_name as a get parameter. Current possible values: Valga1, Aardla"}
    station_data = data_query.get_stations()
    # get the corresponding station entry
    station_entry = None
    for station in station_data["stations"]:
        if station["station_name"] == stationName:
            station_entry = station
            break
    if not station_entry:
        return {"msg:": f"station with name {stationName} was not found!"}
    
    current_weather_data = data_query.get_current(station_entry["station_id"])
    return current_weather_data
    
@bp.route("/stations")
def get_stations_info():
    # get all the different stations available
    data = data_query.get_stations()
    return data
