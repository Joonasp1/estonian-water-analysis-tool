from sqlalchemy.sql.functions import current_timestamp
from sqlalchemy.sql.schema import Column, UniqueConstraint
from sqlalchemy.sql.sqltypes import REAL, TEXT, TIMESTAMP, VARCHAR, Boolean, Integer
from core.db import Base_weather



class Stations(Base_weather):
    __tablename__ = 'stations'
    station_id = Column(Integer, primary_key=True)
    station_name = Column(VARCHAR(20), unique=True)
    product_nr = Column(VARCHAR(20))
    active = Column(Boolean)
    longitude = Column(REAL, nullable=False)
    latitude = Column(REAL, nullable=False)
    elevation = Column(REAL)
    timezone = Column(VARCHAR(50))
    city = Column(VARCHAR(20))
    entry_timestamp = Column(TIMESTAMP, server_default=current_timestamp())


class Weather(Base_weather):
    __tablename__ = "weather_data"
    pk = Column(Integer, primary_key=True)
    station_id = Column(Integer)
    datadesc = Column(VARCHAR(50), nullable=False)
    content = Column(TEXT)
    creation_timestamp = Column(TIMESTAMP, server_default=current_timestamp())