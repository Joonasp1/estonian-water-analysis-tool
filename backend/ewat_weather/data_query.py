# https://weatherlink.github.io/v2-api/tutorial

import hmac
import hashlib
import requests
import time
from datetime import datetime

APIv2_SECRET = "n7sfjeotwyfmrfm9zcpmo2ncza4gvtps"
APIv2_KEY = "gwcccpkeglx3oluskcfbncnlkv7fx8jl"
BASE_URLv2 = "https://api.weatherlink.com/v2/"
# APIv1_TOKEN = "C3509467D8F84730AF4121B0484203B1"

# calculates the api signature by concatenating query parameters
# and then calculating the hmac string using the api secret
def get_api_signature(params_dict):
    concat = ""
    for key in params_dict.keys():
        concat += str(key) + str(params_dict[key])
    
    hmac_str = hmac.new(
        bytes(APIv2_SECRET, "latin-1"),
        msg=bytes(concat, "latin-1"),
        digestmod=hashlib.sha256
    ).hexdigest()
    
    return hmac_str

# adds the api signature to the query params
def add_api_signature(initial_params):
    params = dict(initial_params)
    params["api-signature"] = get_api_signature(initial_params)
    return params


# Performs a request to /stations, returns the result as dict
def get_stations():
    try:
        initial_params = {"api-key": APIv2_KEY, "t": int(time.time())}
        params = add_api_signature(initial_params)
        
        r = requests.get(BASE_URLv2 + "stations", params=params)
        return r.json()
    except requests.exceptions.RequestException as e:
        raise e



# get current weather conditions
def get_current(station_id):
    try:
        params = {"api-key": APIv2_KEY, "station-id": station_id, "t": int(time.time())}
        params = add_api_signature(params)
        del params["station-id"]
        r = requests.get(f"{BASE_URLv2}current/{station_id}", params=params)
        return r.json()
    except requests.exceptions.RequestException as e:
        raise e

# gets the historic weather data of a station between the specified unix timestamps
def get_historic(station_id, start_timestamp, end_timestamp):
    try:
        params = {"api-key": APIv2_KEY, "end-timestamp": end_timestamp, 
            "start-timestamp": start_timestamp, "station-id": station_id, "t": int(time.time())}
        params = add_api_signature(params)
        del params["station-id"]
        r = requests.get(f"{BASE_URLv2}historic/{station_id}", params=params)
        return r.json()
    except requests.exceptions.RequestException as e:
        raise e

# return the unix timestamp of a particular date and time (use format dd-mm-yyyy HH:MM:SS)
def get_unix_timestamp(date_str):
    datetime_obj = datetime.strptime(date_str, "%d-%m-%Y %H:%M:%S")
    return int(datetime_obj.timestamp())


def main():
    import json 
    stations_data = get_stations()
    print(stations_data)
    # There are 2 stations currently: Aardla and Valga1
    station_ids = [station["station_id"] for station in stations_data["stations"]]
    
    current = get_current(station_ids[0])
    # pretty print json
    print(json.dumps(current, indent=2))
    print("\n-----------Historic data-----------\n")
    # seems like one dataset per minute
    historic = get_historic(station_ids[0], get_unix_timestamp("01-09-2021 16:45:00"), get_unix_timestamp("01-09-2021 16:47:00"))
    print(json.dumps(historic, indent=2))


if __name__=="__main__":
    main()