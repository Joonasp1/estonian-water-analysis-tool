from ewat_weather import routes


def init_app(app):
    app.register_blueprint(routes.bp, url_prefix='/weather')
