from types import MethodType
from flask import Blueprint, current_app
from flask import request
from sqlalchemy import func
from geoalchemy2.shape import to_shape
from . import read_abc_rasters
import logging
import pyproj
import json

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

from core.db import db_session
from ewat_land.models import Haritav_maa, Kataster, Hoone, Kolvik_a, Kolvik_ka, Lage, Margala_a, Margala_ka, Muu_rajatis, Ou, Puittaimestik, Seisuveekogu, Tee, Tee_j, Vooluveekogu

bp = Blueprint('ewat_land', __name__)


@bp.route('/hello')
def test():
    current_app.logger.info('Land has been summoned.')
    return 'Hello Land!'

# calculates the land coords based on the geodata in the db
# neccessary when the user only uses the search box
@bp.route('/coords', methods=["GET"])
def get_coords_by_land_id():
    land_id = request.args.get("land_id")
    if not land_id:
        return {"msg": "Please provide land_id as get parameter."}, 400
    
    land = db_session.query(func.st_asgeojson(func.st_centroid(Kataster.geom)).label("centroid")
                                  ).filter(Kataster.tunnus == land_id
                                                  ).first()
    
    coords = json.loads(land["centroid"])["coordinates"]
    
    # convert to EPSG:4326 (used in frontend)
    est=pyproj.CRS("EPSG:3301")
    wgs84=pyproj.CRS("EPSG:4326")
    conv_coords = pyproj.transform(est, wgs84, coords[1], coords[0])
    log.debug(conv_coords)
    return {
        "id": land_id,
        "x": conv_coords[0],
        "y": conv_coords[1]
    }


@bp.route('/abc', methods=["GET"])
def get_abc_route():
    x_coord = request.args.get("xcoord")
    y_coord = request.args.get("ycoord")
    if not (x_coord and y_coord):
        return {"msg": "Please provide xcoord and ycoord as get params"}, 400
    x_coord = float(x_coord)
    y_coord = float(y_coord)
    abc = read_abc_rasters.get_abc(x_coord, y_coord)    
    
    return {
        "x_coord":x_coord, 
        "y_coord":y_coord, 
        "a_param": abc[0],
        "b_param": abc[1],
        "c_param": abc[2]
        }


@bp.route('/area/<string:tunnus>')
def get_areas(tunnus):
    output = {'status' : 'success'}
    try:
        # check cadastre area and check existance
        result = db_session.query(Kataster.tunnus,
                                  func.ST_Area(Kataster.geom).label('katastri_pindala'),
                                  Kataster.geom
                                  ).filter(Kataster.tunnus == tunnus
                                                  ).first()
        cadastre_geom = result['geom']                                                  
        if (result != None):
            output['tunnus'] = result['tunnus']
            output['katastri_pindala'] = result['katastri_pindala']
        else:
            output['status'] = 'fail'
            output['error'] = 'Katastri tunnust ei leitud.'
            return output

        # define different layers and parameters related to them
        geometry_objects = {}
        layers = [
            {'output_label': 'E_202_seisuveekogu', 'model': Seisuveekogu, 'id_label': 'seisuveekogu_etak_id', 'area_label': 'seisuveekogu_pindala', 
            'covers': ['E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a']},
            {'output_label': 'E_203_vooluveekogu', 'model': Vooluveekogu, 'id_label': 'vooluveekogu_etak_id', 'area_label': 'vooluveekogu_pindala', 
            'covers': ['E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a']},
            {'output_label': 'E_301_muu_kolvik_a', 'model': Kolvik_a, 'id_label': 'kolvik_a_etak_id', 'area_label': 'kolvik_a_pindala', 
            'covers': ['E_301_muu_kolvik_ka']},
            {'output_label': 'E_301_muu_kolvik_ka', 'model': Kolvik_ka, 'id_label': 'kolvik_ka_etak_id', 'area_label': 'kolvik_ka_pindala'},
            {'output_label': 'E_302_ou_a', 'model': Ou, 'id_label': 'ou_etak_id', 'area_label': 'ou_pindala',
            'type_label': 'ou_tyyp_t', 'covers': ['E_301_muu_kolvik_ka']},
            {'output_label': 'E_303_haritav_maa_a', 'model': Haritav_maa, 'id_label': 'haritav_maa_etak_id', 'area_label': 'haritav_maa_pindala', 
            'covers': ['E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a']},
            {'output_label': 'E_304_lage_a', 'model': Lage, 'id_label': 'lage_etak_id', 'area_label': 'lage_pindala',
            'type_label': 'lage_tyyp_t', 'covers': ['E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a']},
            {'output_label': 'E_305_puittaimestik_a', 'model': Puittaimestik, 'id_label': 'seisuveekogu_etak_id', 'area_label': 'seisuveekogu_pindala',
            'type_label': 'puittaimestik_tyyp_t', 'covers': ['E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a']},
            {'output_label': 'E_306_margala_a', 'model': Margala_a, 'id_label': 'seisuveekogu_etak_id', 'area_label': 'seisuveekogu_pindala', 
            'covers': ['E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a']},
            {'output_label': 'E_306_margala_ka', 'model': Margala_ka, 'id_label': 'seisuveekogu_etak_id', 'area_label': 'seisuveekogu_pindala', 
            'covers': ['E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a']},
            {'output_label': 'E_401_hoone', 'model': Hoone, 'id_label': 'hoone_etak_id', 'area_label': 'hoone_pindala', 
            'covers': ['E_302_ou_a', 'E_501_tee_a', 'E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a', 'E_305_puittaimestik_a', 'E_304_lage_a']},
            {'output_label': 'E_403_muu_rajatis', 'model': Muu_rajatis, 'id_label': 'muu_rajatis_etak_id', 'area_label': 'muu_rajatis_pindala',
            'covers': ['E_302_ou_a', 'E_501_tee_a', 'E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a', 'E_305_puittaimestik_a', 'E_304_lage_a']},
            {'output_label': 'E_501_tee_a', 'model': Tee, 'id_label': 'tee_etak_id', 'area_label': 'tee_pindala', 
            'covers': ['E_301_muu_kolvik_ka', 'E_301_muu_kolvik_a', 'E_305_puittaimestik_a', 'E_304_lage_a']},
        ]

        # add initial layers
        for layer in layers:
            output[layer['output_label']] = {'objektid': []}
            geometry_objects[layer['output_label']] = []
            add_intersecting_objects(
                geometry_objects, 
                output, layer['output_label'], 
                cadastre_geom, layer['model'], 
                layer['id_label'], layer['area_label'],
                type_label=layer['type_label'] if 'type_label' in layer else None
            )
        
        # check and add layers covering each other
        for layer in layers:
            if 'covers' in layer:
                for covered_layer in layer['covers']:
                    if ('katvad_objektid' not in output[covered_layer]):
                        output[covered_layer]['katvad_objektid'] = []
                    add_covering_objects(output, covered_layer, geometry_objects[layer['output_label']], geometry_objects[covered_layer])
        
        # add most likely road type
        for road in geometry_objects['E_501_tee_a']:
            for roadOutput in output['E_501_tee_a']['objektid']:
                road_line = find_road_line(road['shape_geom'], road['etak_id'])
                if road_line:
                    roadOutput.update(road_line)

    except Exception as e:
        output['status'] = 'fail'
        output['error'] = str(e)
        current_app.logger.error(e)
    
    return output


def add_intersecting_objects(geometry_objects, output, output_label, cadastre_geom, model, id_label, area_label, type_label=None):
    # querys an etak layer in the database and adds the result to output
    query_params = [
        model.etak_id.label(id_label),
        func.ST_Area(func.ST_Intersection(model.geom, cadastre_geom)).label(area_label),
        func.ST_Intersection(model.geom, cadastre_geom).label('shape_object')
    ]

    if type_label is not None:
        query_params.append(model.tyyp_t.label(type_label))

    query = db_session.query(
                *query_params
            ).filter(
                func.ST_Intersects(model.geom, cadastre_geom)
            )
    
    query = query.filter(func.ST_Intersects(model.geom, cadastre_geom))
    
    result = query.all()
    if (len(result) > 0):
        for row in result:
            if (check_add_condition(row[id_label], output[output_label]['objektid'])):
                output[output_label]['objektid'].append(create_area_object(row[id_label], row[area_label], type=row[type_label] if type_label else None))
                geometry_objects[output_label].append({'etak_id': row[id_label], 'shape_object': to_shape(row['shape_object']), 'shape_geom': row['shape_object']})


def add_covering_objects(output, output_label, covering_shapes, covered_shapes):
    # creates all objects covering a defined etak layer
    for covered_shape in covered_shapes:
        for covering_shape in covering_shapes:
            if (covered_shape['shape_object'].intersects(covering_shape['shape_object'])):
                output[output_label]['katvad_objektid'].append(
                    create_area_object(
                        covering_shape['etak_id'],
                        covered_shape['shape_object'].intersection(covering_shape['shape_object']).area,
                        covered_obj=covered_shape['etak_id']
                    )
                )


def create_area_object(id, area, type=None, covered_obj=None, other=None):
    # formats an area object, dict
    output = {'etak_id': int(id), 'pindala': area}
    if (type is not None):
        output["tyyp"] = type
    if (covered_obj is not None):
        output["katetav_objekt"] = int(covered_obj)
    if (other is not None):
        for key in other:
            output[key] = other[key]
    return output


def check_add_condition(id, array):
    return id is not None and not array_contains_object(array, int(id))


def check_covering_obj_condition(id, array, area):
    return id is not None and area is not None and area > 0 and not array_contains_object(array, int(id))


def array_contains_object(array, id):
    return any(id == area_obj['etak_id'] for area_obj in array)


def find_road_line(intersection_geom, road_id):  # used to find the closest road(line) to the intersection of the road(area) and cadastre
    if (intersection_geom is None or road_id is None): return None
    result = db_session.query(
                Tee_j.etak_id,
                Tee_j.teekate_t,
                func.ST_Distance(intersection_geom, Tee_j.geom).label('distance')
            ).filter(
                Tee.etak_id == road_id,
                func.ST_Intersects(Tee.geom, Tee_j.geom)
            ).order_by('distance').limit(1).all()
    if (len(result) == 0): return None
    return {"tee_joone_id": int(result[0]['etak_id']), "tyyp": result[0]['teekate_t']}