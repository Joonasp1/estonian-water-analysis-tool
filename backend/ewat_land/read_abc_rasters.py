import os
import re
import rasterio
import pyproj

path_to_rasters = os.path.abspath("/data")
# min_a = 193
# min_b = 0.315
# min_c = 0.64
max_a = 471
max_b = 0.361
max_c = 0.837

# convertToEPSG3301(58.34790033731437, 26.72977652727955)
def convertToEPSG3301(coordx, coordy):
    wgs84=pyproj.CRS("EPSG:4326")
    est=pyproj.CRS("EPSG:3301")
    return pyproj.transform(wgs84, est, coordx, coordy)



# params: EPSG:4326 cooridinates
# returns: a, b, c parameters as a tuple
def get_abc(coord_x, coord_y):

    coord_x, coord_y = convertToEPSG3301(coord_x, coord_y)

    # don't have a raster for a yet
    a = get_param(max_a, "a_modified.tif", coord_y, coord_x) 
    b = get_param(max_b, "Parameeter_b_modified.tif", coord_y, coord_x)
    c = get_param(max_c, "Parameeter_c_modified.tif", coord_y, coord_x)

    return (a,b,c)
    

def get_param(max_param_value, filename, coord_y, coord_x):
    with rasterio.open(path_to_rasters + "/" + filename) as dataset:
        pixel_x, pixel_y = dataset.index(coord_y, coord_x)
        print(pixel_x, pixel_y)
        band = dataset.read(1)
        scaler = max_param_value / band.max()
        return band[pixel_x, pixel_y] * scaler

