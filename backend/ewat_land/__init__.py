from ewat_land import routes


def init_app(app):
    app.register_blueprint(routes.bp, url_prefix='/land')
