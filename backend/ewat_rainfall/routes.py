from flask import Blueprint, current_app

bp = Blueprint("ewat_rainfall", __name__)


@bp.route("/hello")
def test():
    current_app.logger.info("Rainfall has been summoned.")
    return "Hello Rainfall!"
