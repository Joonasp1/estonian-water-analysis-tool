import json

def test_hello(client):
    response = client.get("land/hello")
    assert response.data == b"Hello Land!"
    response = client.get("weather/hello")
    assert response.data == b"Hello Weather!"
    response = client.get("rainfall/hello")
    assert response.data == b"Hello Rainfall!"

def test_cadastre_normal(client):
    response = client.get("land/area/79501:002:0651")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "E_202_seisuveekogu": {
        "objektid": []
    },
    "E_203_vooluveekogu": {
        "objektid": []
    },
    "E_301_muu_kolvik_a": {
        "katvad_objektid": [
            {
                "etak_id": 7495176,
                "katetav_objekt": 3441240,
                "pindala": 0.0
            },
            {
                "etak_id": 8159498,
                "katetav_objekt": 3441240,
                "pindala": 0.0
            },
            {
                "etak_id": 8159498,
                "katetav_objekt": 8159499,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 3441240,
                "pindala": 2326.3700293252623
            },
            {
                "etak_id": 8159499,
                "pindala": 8.741234979677483
            }
        ]
    },
    "E_301_muu_kolvik_ka": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_302_ou_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_303_haritav_maa_a": {
        "objektid": []
    },
    "E_304_lage_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_305_puittaimestik_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_306_margala_a": {
        "objektid": []
    },
    "E_306_margala_ka": {
        "objektid": []
    },
    "E_401_hoone": {
        "objektid": [
            {
                "etak_id": 7783177,
                "pindala": 1036.7828000070647
            },
            {
                "etak_id": 7495176,
                "pindala": 4966.926328883251
            }
        ]
    },
    "E_403_muu_rajatis": {
        "objektid": []
    },
    "E_501_tee_a": {
        "katvad_objektid": [
            {
                "etak_id": 7783177,
                "katetav_objekt": 8159498,
                "pindala": 1036.7828000070645
            },
            {
                "etak_id": 7495176,
                "katetav_objekt": 8159498,
                "pindala": 4966.926328883251
            }
        ],
        "objektid": [
            {
                "etak_id": 6593477,
                "pindala": 96.11962363677421,
                "tee_joone_id": 4917475,
                "tyyp": "Püsikate"
            },
            {
                "etak_id": 8159498,
                "pindala": 11331.862011616635,
                "tee_joone_id": 4917475,
                "tyyp": "Püsikate"
            }
        ]
    },
    "katastri_pindala": 13763.092899556954,
    "status": "success",
    "tunnus": "79501:002:0651"
}""")

def test_cadastre_big(client):
    response = client.get("land/area/79501:032:0002")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "E_202_seisuveekogu": {
        "objektid": [
            {
                "etak_id": 2014995,
                "pindala": 25061.133509647803
            }
        ]
    },
    "E_203_vooluveekogu": {
        "objektid": []
    },
    "E_301_muu_kolvik_a": {
        "katvad_objektid": [
            {
                "etak_id": 6924235,
                "katetav_objekt": 3443004,
                "pindala": 0.0
            },
            {
                "etak_id": 3851005,
                "katetav_objekt": 3443018,
                "pindala": 0.0
            },
            {
                "etak_id": 6924233,
                "katetav_objekt": 3830566,
                "pindala": 0.0
            },
            {
                "etak_id": 4042593,
                "katetav_objekt": 3443018,
                "pindala": 0.0
            },
            {
                "etak_id": 7693579,
                "katetav_objekt": 3830566,
                "pindala": 23.09525580838089
            },
            {
                "etak_id": 7752738,
                "katetav_objekt": 8134767,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 8134767,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 3443004,
                "pindala": 0.0
            },
            {
                "etak_id": 7752738,
                "katetav_objekt": 3443063,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 3443063,
                "pindala": 0.0
            },
            {
                "etak_id": 6924444,
                "katetav_objekt": 3830566,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 3830566,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 3443018,
                "pindala": 0.0
            },
            {
                "etak_id": 6924444,
                "katetav_objekt": 6924445,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 8134767,
                "pindala": 0.18683996002285383
            },
            {
                "etak_id": 3443004,
                "pindala": 37.16968146085696
            },
            {
                "etak_id": 3443063,
                "pindala": 9.471016947457255
            },
            {
                "etak_id": 3830566,
                "pindala": 99657.04391459374
            },
            {
                "etak_id": 3443018,
                "pindala": 694.9307974494521
            },
            {
                "etak_id": 6924445,
                "pindala": 1675.545216143426
            }
        ]
    },
    "E_301_muu_kolvik_ka": {
        "katvad_objektid": [
            {
                "etak_id": 3830566,
                "katetav_objekt": 6924624,
                "pindala": 88587.57293558167
            },
            {
                "etak_id": 6924445,
                "katetav_objekt": 6924624,
                "pindala": 1675.545216143426
            },
            {
                "etak_id": 6924233,
                "katetav_objekt": 6924624,
                "pindala": 749.1575510407397
            },
            {
                "etak_id": 7693579,
                "katetav_objekt": 6924624,
                "pindala": 115.78543300898392
            },
            {
                "etak_id": 6924444,
                "katetav_objekt": 6924624,
                "pindala": 2916.007386121956
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 6924624,
                "pindala": 1838.5174144246062
            }
        ],
        "objektid": [
            {
                "etak_id": 6924624,
                "pindala": 95766.80050331399
            }
        ]
    },
    "E_302_ou_a": {
        "katvad_objektid": [
            {
                "etak_id": 712232,
                "katetav_objekt": 3953566,
                "pindala": 4.331557446075381
            },
            {
                "etak_id": 712231,
                "katetav_objekt": 3953566,
                "pindala": 7.601556074955074
            }
        ],
        "objektid": [
            {
                "etak_id": 8134443,
                "pindala": 143.48163833847144,
                "tyyp": "Eraõu"
            },
            {
                "etak_id": 3953889,
                "pindala": 27.557201735858456,
                "tyyp": "Eraõu"
            },
            {
                "etak_id": 3953573,
                "pindala": 0.382503416588861,
                "tyyp": "Eraõu"
            },
            {
                "etak_id": 3953566,
                "pindala": 39.60382788648267,
                "tyyp": "Eraõu"
            },
            {
                "etak_id": 3953557,
                "pindala": 154.749324600204,
                "tyyp": "Eraõu"
            }
        ]
    },
    "E_303_haritav_maa_a": {
        "objektid": []
    },
    "E_304_lage_a": {
        "katvad_objektid": [
            {
                "etak_id": 712232,
                "katetav_objekt": 3841642,
                "pindala": 0.0
            },
            {
                "etak_id": 712231,
                "katetav_objekt": 3841642,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 3851005,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 3659092,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 6924235,
                "pindala": 0.0
            },
            {
                "etak_id": 6966729,
                "katetav_objekt": 3841642,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 3841642,
                "pindala": 0.0
            },
            {
                "etak_id": 7752740,
                "katetav_objekt": 6924225,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 3582890,
                "pindala": 184.11025312916507,
                "tyyp": "Liivane ala"
            },
            {
                "etak_id": 3851005,
                "pindala": 15266.705225408163,
                "tyyp": "Rohumaa"
            },
            {
                "etak_id": 3659092,
                "pindala": 519.4022760661663,
                "tyyp": "Rohumaa"
            },
            {
                "etak_id": 6924235,
                "pindala": 846.0407488332028,
                "tyyp": "Rohumaa"
            },
            {
                "etak_id": 3841642,
                "pindala": 1858.1068086636253,
                "tyyp": "Rohumaa"
            },
            {
                "etak_id": 6924225,
                "pindala": 1070.4175149597604,
                "tyyp": "Rohumaa"
            }
        ]
    },
    "E_305_puittaimestik_a": {
        "katvad_objektid": [],
        "objektid": [
            {
                "etak_id": 6924233,
                "pindala": 749.1575510407397,
                "tyyp": "Põõsastik"
            }
        ]
    },
    "E_306_margala_a": {
        "objektid": [
            {
                "etak_id": 4042593,
                "pindala": 19075.94036520055
            }
        ]
    },
    "E_306_margala_ka": {
        "objektid": []
    },
    "E_401_hoone": {
        "objektid": [
            {
                "etak_id": 712232,
                "pindala": 4.331557446686455
            },
            {
                "etak_id": 712231,
                "pindala": 7.601556075147762
            }
        ]
    },
    "E_403_muu_rajatis": {
        "objektid": [
            {
                "etak_id": 7693579,
                "pindala": 115.78543300898392
            }
        ]
    },
    "E_501_tee_a": {
        "katvad_objektid": [
            {
                "etak_id": 7693579,
                "katetav_objekt": 6924444,
                "pindala": 92.6901771956381
            }
        ],
        "objektid": [
            {
                "etak_id": 6966729,
                "pindala": 112.48994579695797,
                "tee_joone_id": 5056411,
                "tyyp": "Kruuskate"
            },
            {
                "etak_id": 7752738,
                "pindala": 4.12794888524701,
                "tee_joone_id": 5056411,
                "tyyp": "Kruuskate"
            },
            {
                "etak_id": 6924444,
                "pindala": 2916.0073861219557,
                "tee_joone_id": 5056411,
                "tyyp": "Kruuskate"
            },
            {
                "etak_id": 7752740,
                "pindala": 8178.511252913325,
                "tee_joone_id": 5056411,
                "tyyp": "Kruuskate"
            }
        ]
    },
    "katastri_pindala": 178282.27274919784,
    "status": "success",
    "tunnus": "79501:032:0002"
}""")

def test_cadastre_water(client):
    response = client.get("land/area/79301:001:0052")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "E_202_seisuveekogu": {
        "objektid": [
            {
                "etak_id": 2015952,
                "pindala": 94313.92771617863
            }
        ]
    },
    "E_203_vooluveekogu": {
        "objektid": []
    },
    "E_301_muu_kolvik_a": {
        "katvad_objektid": [
            {
                "etak_id": 2015952,
                "katetav_objekt": 3444086,
                "pindala": 0.0
            },
            {
                "etak_id": 3722457,
                "katetav_objekt": 3444086,
                "pindala": 0.0
            },
            {
                "etak_id": 3610473,
                "katetav_objekt": 3444086,
                "pindala": 0.0
            },
            {
                "etak_id": 3610473,
                "katetav_objekt": 3444085,
                "pindala": 0.0
            },
            {
                "etak_id": 4042604,
                "katetav_objekt": 6924172,
                "pindala": 0.0
            },
            {
                "etak_id": 6593477,
                "katetav_objekt": 3444062,
                "pindala": 0.0
            },
            {
                "etak_id": 6593477,
                "katetav_objekt": 3444064,
                "pindala": 0.0
            },
            {
                "etak_id": 6593477,
                "katetav_objekt": 3444086,
                "pindala": 0.0
            },
            {
                "etak_id": 6593477,
                "katetav_objekt": 6924172,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 3444062,
                "pindala": 334.5553947416977
            },
            {
                "etak_id": 3444064,
                "pindala": 378.86949546212213
            },
            {
                "etak_id": 3444086,
                "pindala": 30814.373223607494
            },
            {
                "etak_id": 3444085,
                "pindala": 0.0382179954692603
            },
            {
                "etak_id": 6924172,
                "pindala": 4068.8972542812116
            }
        ]
    },
    "E_301_muu_kolvik_ka": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_302_ou_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_303_haritav_maa_a": {
        "objektid": []
    },
    "E_304_lage_a": {
        "katvad_objektid": [
            {
                "etak_id": 6498179,
                "katetav_objekt": 3610473,
                "pindala": 23.21872999855548
            },
            {
                "etak_id": 6498178,
                "katetav_objekt": 3610473,
                "pindala": 28.93615500270881
            },
            {
                "etak_id": 6593477,
                "katetav_objekt": 3610473,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 3761627,
                "pindala": 4804.552379920018,
                "tyyp": "Muu lage"
            },
            {
                "etak_id": 3722457,
                "pindala": 4709.181547274285,
                "tyyp": "Liivane ala"
            },
            {
                "etak_id": 3610473,
                "pindala": 102755.7605699941,
                "tyyp": "Rohumaa"
            }
        ]
    },
    "E_305_puittaimestik_a": {
        "katvad_objektid": [
            {
                "etak_id": 6593477,
                "katetav_objekt": 4042604,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 4042604,
                "pindala": 479.7898173581646,
                "tyyp": "Põõsastik"
            }
        ]
    },
    "E_306_margala_a": {
        "objektid": []
    },
    "E_306_margala_ka": {
        "objektid": []
    },
    "E_401_hoone": {
        "objektid": []
    },
    "E_403_muu_rajatis": {
        "objektid": [
            {
                "etak_id": 6498179,
                "pindala": 23.21872999855548
            },
            {
                "etak_id": 6498178,
                "pindala": 28.93615500270881
            }
        ]
    },
    "E_501_tee_a": {
        "katvad_objektid": [],
        "objektid": [
            {
                "etak_id": 6593477,
                "pindala": 8822.08823279867,
                "tee_joone_id": 4665639,
                "tyyp": "Püsikate"
            }
        ]
    },
    "katastri_pindala": 251482.03384955256,
    "status": "success",
    "tunnus": "79301:001:0052"
}""")

def test_cadastre_no_buildings(client):
    response = client.get("land/area/43201:001:0474")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "E_202_seisuveekogu": {
        "objektid": []
    },
    "E_203_vooluveekogu": {
        "objektid": []
    },
    "E_301_muu_kolvik_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_301_muu_kolvik_ka": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_302_ou_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_303_haritav_maa_a": {
        "objektid": [
            {
                "etak_id": 8241590,
                "pindala": 108244.3342788282
            }
        ]
    },
    "E_304_lage_a": {
        "katvad_objektid": [],
        "objektid": [
            {
                "etak_id": 3629822,
                "pindala": 207.10311853626646,
                "tyyp": "Muu lage"
            },
            {
                "etak_id": 3655346,
                "pindala": 8.748090866808544,
                "tyyp": "Rohumaa"
            }
        ]
    },
    "E_305_puittaimestik_a": {
        "katvad_objektid": [],
        "objektid": [
            {
                "etak_id": 3373996,
                "pindala": 46591.239291870465,
                "tyyp": "Mets"
            },
            {
                "etak_id": 3183968,
                "pindala": 1116.5871190702062,
                "tyyp": "Mets"
            }
        ]
    },
    "E_306_margala_a": {
        "objektid": []
    },
    "E_306_margala_ka": {
        "objektid": []
    },
    "E_401_hoone": {
        "objektid": []
    },
    "E_403_muu_rajatis": {
        "objektid": []
    },
    "E_501_tee_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "katastri_pindala": 156168.01189915213,
    "status": "success",
    "tunnus": "43201:001:0474"
}""")

def test_cadastre_complex(client):
    response = client.get("land/area/79507:023:0022")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "E_202_seisuveekogu": {
        "objektid": []
    },
    "E_203_vooluveekogu": {
        "objektid": []
    },
    "E_301_muu_kolvik_a": {
        "katvad_objektid": [
            {
                "etak_id": 6593469,
                "katetav_objekt": 3441394,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 3441394,
                "pindala": 1158.897015480607
            }
        ]
    },
    "E_301_muu_kolvik_ka": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_302_ou_a": {
        "katvad_objektid": [
            {
                "etak_id": 725831,
                "katetav_objekt": 3952695,
                "pindala": 0.4086265342650282
            },
            {
                "etak_id": 644804,
                "katetav_objekt": 3952695,
                "pindala": 10.77591780397145
            },
            {
                "etak_id": 644805,
                "katetav_objekt": 3952695,
                "pindala": 2.273958708132737
            },
            {
                "etak_id": 715496,
                "katetav_objekt": 3952695,
                "pindala": 3.6737027894688703
            },
            {
                "etak_id": 645148,
                "katetav_objekt": 3952700,
                "pindala": 8.050397479005301
            },
            {
                "etak_id": 719828,
                "katetav_objekt": 3952655,
                "pindala": 4.092089421140017
            }
        ],
        "objektid": [
            {
                "etak_id": 3952695,
                "pindala": 22.758085407346186,
                "tyyp": "Eraõu"
            },
            {
                "etak_id": 3952656,
                "pindala": 41.465194393487735,
                "tyyp": "Eraõu"
            },
            {
                "etak_id": 3952700,
                "pindala": 8.050397479019738,
                "tyyp": "Eraõu"
            },
            {
                "etak_id": 3952655,
                "pindala": 30.455818799364195,
                "tyyp": "Eraõu"
            }
        ]
    },
    "E_303_haritav_maa_a": {
        "objektid": []
    },
    "E_304_lage_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_305_puittaimestik_a": {
        "katvad_objektid": [
            {
                "etak_id": 645148,
                "katetav_objekt": 3376913,
                "pindala": 0.00015357204233796732
            },
            {
                "etak_id": 728416,
                "katetav_objekt": 3376913,
                "pindala": 25.505194697297934
            },
            {
                "etak_id": 725831,
                "katetav_objekt": 3376913,
                "pindala": 0.0
            },
            {
                "etak_id": 719730,
                "katetav_objekt": 3376913,
                "pindala": 0.9374215586606043
            },
            {
                "etak_id": 711874,
                "katetav_objekt": 3376913,
                "pindala": 5.630751840753213
            },
            {
                "etak_id": 644804,
                "katetav_objekt": 3376913,
                "pindala": 0.0014873738050575503
            },
            {
                "etak_id": 644805,
                "katetav_objekt": 3376913,
                "pindala": 0.000982634545949098
            },
            {
                "etak_id": 715496,
                "katetav_objekt": 3376913,
                "pindala": 0.0
            },
            {
                "etak_id": 719828,
                "katetav_objekt": 3376913,
                "pindala": 0.0
            },
            {
                "etak_id": 724242,
                "katetav_objekt": 3376913,
                "pindala": 1.9263807190384394
            },
            {
                "etak_id": 720524,
                "katetav_objekt": 3376913,
                "pindala": 1.1596670459776246
            },
            {
                "etak_id": 734391,
                "katetav_objekt": 3376913,
                "pindala": 3.862427385524218
            },
            {
                "etak_id": 1953427,
                "katetav_objekt": 3376913,
                "pindala": 0.0
            },
            {
                "etak_id": 6593469,
                "katetav_objekt": 3376913,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 3376913,
                "pindala": 22934.428577139835,
                "tyyp": "Mets"
            }
        ]
    },
    "E_306_margala_a": {
        "objektid": []
    },
    "E_306_margala_ka": {
        "objektid": []
    },
    "E_401_hoone": {
        "objektid": [
            {
                "etak_id": 645148,
                "pindala": 8.05055105104571
            },
            {
                "etak_id": 728416,
                "pindala": 25.505194697297927
            },
            {
                "etak_id": 725831,
                "pindala": 0.4086265342650282
            },
            {
                "etak_id": 719730,
                "pindala": 0.9374215587922264
            },
            {
                "etak_id": 711874,
                "pindala": 5.630751840753213
            },
            {
                "etak_id": 644804,
                "pindala": 10.777405177783056
            },
            {
                "etak_id": 644805,
                "pindala": 2.2749413428943397
            },
            {
                "etak_id": 715496,
                "pindala": 3.6737057222438976
            },
            {
                "etak_id": 719828,
                "pindala": 4.09208942114002
            },
            {
                "etak_id": 724242,
                "pindala": 1.9263807190384385
            },
            {
                "etak_id": 720524,
                "pindala": 1.1596670461990475
            }
        ]
    },
    "E_403_muu_rajatis": {
        "objektid": [
            {
                "etak_id": 734391,
                "pindala": 3.862427385524218
            }
        ]
    },
    "E_501_tee_a": {
        "katvad_objektid": [
            {
                "etak_id": 645148,
                "katetav_objekt": 6593469,
                "pindala": 0.0
            },
            {
                "etak_id": 715496,
                "katetav_objekt": 6593469,
                "pindala": 2.9324371836676093e-06
            }
        ],
        "objektid": [
            {
                "etak_id": 1953427,
                "pindala": 501.9231660349827,
                "tee_joone_id": 4777677,
                "tyyp": "Püsikate"
            },
            {
                "etak_id": 6593469,
                "pindala": 906.121195436971,
                "tee_joone_id": 4777677,
                "tyyp": "Püsikate"
            }
        ]
    },
    "katastri_pindala": 25604.099450184432,
    "status": "success",
    "tunnus": "79507:023:0022"
}""")

def test_cadastre_complex2(client):
    response = client.get("land/area/79507:001:0002")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "E_202_seisuveekogu": {
        "objektid": [
            {
                "etak_id": 2015763,
                "pindala": 3447.0296181427407
            }
        ]
    },
    "E_203_vooluveekogu": {
        "objektid": []
    },
    "E_301_muu_kolvik_a": {
        "katvad_objektid": [
            {
                "etak_id": 2015763,
                "katetav_objekt": 3441118,
                "pindala": 0.0
            },
            {
                "etak_id": 716908,
                "katetav_objekt": 3441118,
                "pindala": 682.6517904897477
            },
            {
                "etak_id": 6244351,
                "katetav_objekt": 3441118,
                "pindala": 50.27249095194081
            },
            {
                "etak_id": 718123,
                "katetav_objekt": 3441118,
                "pindala": 1124.6667574667933
            },
            {
                "etak_id": 644004,
                "katetav_objekt": 3441118,
                "pindala": 0.00033730770142881283
            },
            {
                "etak_id": 8452546,
                "katetav_objekt": 3441118,
                "pindala": 0.001912353835657879
            },
            {
                "etak_id": 6593471,
                "katetav_objekt": 3441118,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 3441118,
                "pindala": 25613.450713028058
            }
        ]
    },
    "E_301_muu_kolvik_ka": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_302_ou_a": {
        "katvad_objektid": [
            {
                "etak_id": 644004,
                "katetav_objekt": 3952582,
                "pindala": 0.009424091239392099
            },
            {
                "etak_id": 8452546,
                "katetav_objekt": 3952582,
                "pindala": 3.0426474729494553
            }
        ],
        "objektid": [
            {
                "etak_id": 3952582,
                "pindala": 3.0520715647890087,
                "tyyp": "Eraõu"
            }
        ]
    },
    "E_303_haritav_maa_a": {
        "objektid": []
    },
    "E_304_lage_a": {
        "katvad_objektid": [],
        "objektid": [
            {
                "etak_id": 3641678,
                "pindala": 473.1655705735454,
                "tyyp": "Rohumaa"
            }
        ]
    },
    "E_305_puittaimestik_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_306_margala_a": {
        "objektid": []
    },
    "E_306_margala_ka": {
        "objektid": []
    },
    "E_401_hoone": {
        "objektid": [
            {
                "etak_id": 716908,
                "pindala": 682.6517904897476
            },
            {
                "etak_id": 6244351,
                "pindala": 50.27249095194081
            },
            {
                "etak_id": 718123,
                "pindala": 1124.6667574667936
            },
            {
                "etak_id": 644004,
                "pindala": 0.009761398868520663
            },
            {
                "etak_id": 8452546,
                "pindala": 3.04455982903101
            }
        ]
    },
    "E_403_muu_rajatis": {
        "objektid": []
    },
    "E_501_tee_a": {
        "katvad_objektid": [
            {
                "etak_id": 716908,
                "katetav_objekt": 6593471,
                "pindala": 0.0
            },
            {
                "etak_id": 6244351,
                "katetav_objekt": 6593471,
                "pindala": 0.0
            },
            {
                "etak_id": 718123,
                "katetav_objekt": 6593471,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 6593471,
                "pindala": 1515.140376609882,
                "tee_joone_id": 7227568,
                "tyyp": "Kruuskate"
            }
        ]
    },
    "katastri_pindala": 31051.83834990674,
    "status": "success",
    "tunnus": "79507:001:0002"
}""")

def test_cadastre_river(client):
    response = client.get("land/area/79301:001:0687")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "E_202_seisuveekogu": {
        "objektid": []
    },
    "E_203_vooluveekogu": {
        "objektid": [
            {
                "etak_id": 7009519,
                "pindala": 64036.60345691396
            }
        ]
    },
    "E_301_muu_kolvik_a": {
        "katvad_objektid": [
            {
                "etak_id": 7009519,
                "katetav_objekt": 3444085,
                "pindala": 0.0
            },
            {
                "etak_id": 3610473,
                "katetav_objekt": 3444085,
                "pindala": 0.0
            }
        ],
        "objektid": [
            {
                "etak_id": 3444085,
                "pindala": 366.28127379392805
            }
        ]
    },
    "E_301_muu_kolvik_ka": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_302_ou_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_303_haritav_maa_a": {
        "objektid": []
    },
    "E_304_lage_a": {
        "katvad_objektid": [],
        "objektid": [
            {
                "etak_id": 8391072,
                "pindala": 2880.4887225323782,
                "tyyp": "Muu lage"
            },
            {
                "etak_id": 3610473,
                "pindala": 6376.346846237473,
                "tyyp": "Rohumaa"
            }
        ]
    },
    "E_305_puittaimestik_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "E_306_margala_a": {
        "objektid": []
    },
    "E_306_margala_ka": {
        "objektid": []
    },
    "E_401_hoone": {
        "objektid": []
    },
    "E_403_muu_rajatis": {
        "objektid": [
            {
                "etak_id": 734666,
                "pindala": 110.36523002534031
            },
            {
                "etak_id": 734646,
                "pindala": 241.11418748934562
            },
            {
                "etak_id": 734658,
                "pindala": 188.99290829518705
            },
            {
                "etak_id": 725258,
                "pindala": 571.4543513306821
            }
        ]
    },
    "E_501_tee_a": {
        "katvad_objektid": [],
        "objektid": []
    },
    "katastri_pindala": 73659.72029949776,
    "status": "success",
    "tunnus": "79301:001:0687"
}""")

def test_coords(client):
    response = client.get("land/coords?land_id=61901:002:0250")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "id": "61901:002:0250",
    "x": 58.06349048952412,
    "y": 26.950045594761065
}""")

def test_weather_history(client):
    response = client.get("weather/history?station_id=17648&start=2021-12-01 18:00:00&end=2021-12-01 20:00:00&parameter=temp_out")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "data": [
        {
            "content": "19.4",
            "creation_timestamp": "Wed, 01 Dec 2021 18:55:29 GMT",
            "datadesc": "temp_out"
        },
        {
            "content": "18.9",
            "creation_timestamp": "Wed, 01 Dec 2021 19:05:31 GMT",
            "datadesc": "temp_out"
        },
        {
            "content": "18.1",
            "creation_timestamp": "Wed, 01 Dec 2021 19:51:51 GMT",
            "datadesc": "temp_out"
        },
        {
            "content": "16.9",
            "creation_timestamp": "Wed, 01 Dec 2021 19:59:58 GMT",
            "datadesc": "temp_out"
        }
    ],
    "station_id": "17648",
    "status": "success"
}""")


def test_weather_params(client):
    response = client.get("weather/params?station_id=17648")
    assert json.loads(response.data.decode("utf-8")) == json.loads("""{
    "params": [
        "hum_out",
        "wind_speed_10_min_avg",
        "wind_dir",
        "rain_day_mm",
        "bar",
        "temp_out"
    ],
    "station_id": "17648",
    "status": "success"
}""")

def test_weather_stations(client):
    response = client.get("weather/stations")
    responseJson = json.loads(response.data.decode("utf-8"))
    responseJson.pop("generated_at", None)
    correctJson = json.loads("""{
    "generated_at": 1638811027,
    "stations": [
        {
            "active": true,
            "city": "Tartu",
            "company_name": "",
            "country": "Estonia",
            "elevation": 244.40703,
            "firmware_version": "1.1.5",
            "gateway_id": 65355,
            "gateway_id_hex": "001D0A00FF4B",
            "latitude": 58.35385,
            "longitude": 26.69303,
            "private": false,
            "product_number": "6555",
            "recording_interval": 1,
            "region": "Tartu maakond",
            "registered_date": 1534869110,
            "station_id": 17648,
            "station_name": "Aardla_95",
            "time_zone": "Europe/Tallinn",
            "user_email": "mortenx83@gmail.com",
            "username": "mortenmo"
        },
        {
            "active": true,
            "city": "Valga",
            "company_name": "",
            "country": "Estonia",
            "elevation": 143.67563,
            "firmware_version": null,
            "gateway_id": 7423330,
            "gateway_id_hex": "001D0A714562",
            "latitude": 57.78166,
            "longitude": 26.04016,
            "private": false,
            "product_number": "6100USB",
            "recording_interval": 1,
            "region": "Valga maakond",
            "registered_date": 1617983884,
            "station_id": 111341,
            "station_name": "Valga_keskjaam",
            "time_zone": "Europe/Tallinn",
            "user_email": "mortenx83@gmail.com",
            "username": "mortenmo"
        }
    ]
}""")
    correctJson.pop("generated_at", None)
    assert responseJson == correctJson

def test_weather_current_aardla(client):
    response = client.get("weather/current?station_name=Aardla_95")
    responseJson = json.loads(response.data.decode("utf-8"))
    assert 'generated_at' in responseJson
    assert responseJson['station_id'] == 17648
    assert 'sensors' in responseJson
    assert 'temp_out' in responseJson["sensors"][0]["data"][0]
    assert 'hum_out' in responseJson["sensors"][0]["data"][0]
    assert 'wind_speed' in responseJson["sensors"][0]["data"][0]
    assert 'bar' in responseJson["sensors"][0]["data"][0]
    assert 'rain_day_mm' in responseJson["sensors"][0]["data"][0]
    assert 'wind_dir' in responseJson["sensors"][0]["data"][0]

def test_weather_current_valga(client):
    response = client.get("weather/current?station_name=Valga_keskjaam")
    responseJson = json.loads(response.data.decode("utf-8"))
    assert 'generated_at' in responseJson
    assert responseJson['station_id'] == 111341
    assert 'sensors' in responseJson
    assert 'temp' in responseJson["sensors"][4]["data"][0]
    assert 'hum' in responseJson["sensors"][4]["data"][0]
    assert 'wind_speed_last' in responseJson["sensors"][4]["data"][0]
    assert 'rainfall_daily_mm' in responseJson["sensors"][4]["data"][0]
    assert 'wind_dir_last' in responseJson["sensors"][4]["data"][0]