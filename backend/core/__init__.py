import logging
import os

from flask import Flask
import ewat_land
import ewat_rainfall
import ewat_weather
from . import db, routes

def create_app(config=None):
    # create the app
    app = Flask(__name__)

    # load default configuration
    app.config.from_object('config.settings')
    # load environment configuration
    if 'FLASK_CONF' in os.environ:
        app.config.from_envvar('FLASK_CONF')
    # load app specified configuration
    if config is not None:
        if isinstance(config, dict):
            app.config.update(config)
        elif config.endswith('.py'):
            app.config.from_pyfile(config)

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.shutdown_session()

    # changed url prefix from /api to / (/api comes from nginx conf)
    # localhost/api/...
    app.register_blueprint(routes.bp, url_prefix='/')
    db.init_db(app.config.get("TESTING", False))
    ewat_land.init_app(app)
    ewat_rainfall.init_app(app)
    ewat_weather.init_app(app)

    logging.basicConfig(filename='record.log', level=logging.INFO,
                        format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

    return app
