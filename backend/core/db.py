from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

db_session = scoped_session(sessionmaker(autocommit=False,
                                            autoflush=False))
db_weather_session = scoped_session(sessionmaker(autocommit=False,
                                            autoflush=False))

Base = declarative_base()
Base_weather = declarative_base()

def init_db(testing=None):
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    ewat_db_url = 'postgresql://postgres:testpass@postgis:5432/EWAT'
    weather_db_url = 'postgresql://postgres:loggerdbpass@weather_db:5432/EWAT_WEATHER'
    if testing:
        ewat_db_url = 'postgresql://postgres:testpass@165.232.114.175:5432/EWAT'
        weather_db_url = 'postgresql://postgres:loggerdbpass@165.232.114.175:5434/EWAT_WEATHER'

    engine = create_engine(ewat_db_url)
    engine_weather = create_engine(weather_db_url)

    db_session.configure(bind=engine)
    db_weather_session.configure(bind=engine_weather)

    Base.query = db_session.query_property()
    Base_weather.query = db_weather_session.query_property()

    Base.metadata.create_all(bind=engine)
    Base_weather.metadata.create_all(bind=engine_weather)


def shutdown_session(exception=None):
    db_session.remove()
    db_weather_session.remove()