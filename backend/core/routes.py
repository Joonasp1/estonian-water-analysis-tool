from flask import Blueprint

bp = Blueprint("log", __name__)


@bp.route("/log")
def get_log():
    with open('record.log') as f:
        log = f.read()
    return log.replace("\n", "<br>")

