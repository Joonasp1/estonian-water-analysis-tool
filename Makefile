.PHONY: frontend
.PHONY: backend

backend:
	docker build -t backend ./backend/
	docker build -t w_logger ./weather_logger/

frontend:
	cd ./frontend && npm run build
	docker build -t frontend ./frontend/

build: backend frontend

deploy:
	docker-compose up -d

down:
	docker-compose down

redeploy: build deploy

clean: down
	# delete all containers
	- docker rm -f $(shell docker ps -a -q)
	# delete all volumes
	- docker volume rm $(shell docker volume ls -q)

deploy_production:
	docker-compose -f docker-compose-production.yml up -d


