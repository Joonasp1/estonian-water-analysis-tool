import { mount, createLocalVue } from '@vue/test-utils'
import MapComponent from '@/components/MapComponent.vue'
import Vuex from 'vuex'

//window.URL.createObjectURL = function() {};
const localVue = createLocalVue();
localVue.use(Vuex);

//Create dummy store
const store = new Vuex.Store({
    state: {
        areas: {
            totalSurface: 0,
            roofSurface: 0,
            grassSurface: 0,
            seisuVeekogu: 0,
            vooluVeekogu: 0,
            muuKolvik: 0,
            ou: 0,
            haritavMaa: 0,
            lage: 0,
            puitTaimestik: 0,
            margalaA: 0,
            margalaKa: 0,
            hoone: 0,
            muuRajatis: 0,
            tee: 0,
        },
        plotNumber: "",
        loading: false,
        coords: {"x":58.22, "y":26.43} // click coords
    },
    mutations: {
        changeCoords(state, {x, y}){
            state.coords["x"] = x
            state.coords["y"] = y
        },
        setLoading(state, value){
            state.loading = value
        },
        changePlotNumber(state,value){
            state.plotNumber = value
        },
        changeAreas(state, {area, value}){
            state.areas[area] = value
        }    
    },
    getters: {
        getPlotNumber: state => {
            return state.plotNumber
        },
        getLoading: state => {
            return state.loading
        },
        getCoords: state => {
            return state.coords
        },
        getAreas: state => {
            return state.areas
        }
    }
});

describe('MapComponent', () => {

    const wrapper = mount(MapComponent, {store, localVue})

    it('Renders correctly', () =>{

        const inputitem = wrapper.find(".map");
        expect(inputitem.exists()).toBe(true);
        
    })
})