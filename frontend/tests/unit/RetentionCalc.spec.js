// import {shallowmount} from '@vue/test-utils'
import { mount, createLocalVue } from '@vue/test-utils'
import RetentionCalc from '@/components/RetentionCalc.vue'
import Vuex from 'vuex'
import { config } from '@vue/test-utils'
config.mocks["$t"] = () => {}

const localVue = createLocalVue();
localVue.use(Vuex);

//Create dummy store
const store = new Vuex.Store({
    state: {
        areas: {
            totalSurface: 0,
            roofSurface: 0,
            grassSurface: 0,
            seisuVeekogu: 0,
            vooluVeekogu: 0,
            muuKolvik: 0,
            ou: 0,
            haritavMaa: 0,
            lage: 0,
            puitTaimestik: 0,
            margalaA: 0,
            margalaKa: 0,
            hoone: 0,
            muuRajatis: 0,
            tee: 0,
        },
        plotNumber: "",
        loading: false,
        coords: {"x":58.22, "y":26.43} // click coords
    },
    mutations: {
        changeCoords(state, {x, y}){
            state.coords["x"] = x
            state.coords["y"] = y
        },
        setLoading(state, value){
            state.loading = value
        },
        changePlotNumber(state,value){
            state.plotNumber = value
        },
        changeAreas(state, {area, value}){
            state.areas[area] = value
        }    
    },
    getters: {
        getPlotNumber: state => {
            return state.plotNumber
        },
        getLoading: state => {
            return state.loading
        },
        getCoords: state => {
            return state.coords
        },
        getAreas: state => {
            return state.areas
        }
    }
});

describe('RetentionCalc', () => {

    const wrapper = mount(RetentionCalc, {store, localVue})

    it('Renders correctly', () =>{

        const inputitem = wrapper.find(".input");
        expect(inputitem.exists()).toBe(true);
        
    })

    it('Vuex commit and getter works', () =>{

        store.commit("changeAreas",{area: "totalSurface", value:500})
        const totalValue = wrapper.find("totalSurface")
        expect(store.getters.getAreas["totalSurface"]).toBe(500)

    })
})
