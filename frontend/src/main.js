import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueRouter from 'vue-router'
import { i18n } from './lib/i18n'
import store from "./store/index"
import PrimeVue from 'primevue/config'
import 'primevue/resources/primevue.min.css';
import 'primevue/resources/themes/saga-blue/theme.css';
import 'primeicons/primeicons.css';
import inputtext from 'primevue/inputtext';
import Slider from 'primevue/slider';
import Button from 'primevue/button';

Vue.config.productionTip = false

Vue.use(VueRouter);

Vue.use(PrimeVue);
Vue.component('InputText', inputtext);
Vue.component('Slider', Slider);
Vue.component('Button', Button);

new Vue({
  store: store,
  router: router,
  i18n,
  render: h => h(App),
}).$mount('#app')
