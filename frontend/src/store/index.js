import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        areas: {
            totalSurface: 0,
            roofSurface: 0,
            grassSurface: 0,
            seisuVeekogu: 0,
            vooluVeekogu: 0,
            muuKolvik: 0,
            ou: 0,
            haritavMaa: 0,
            lage: 0,
            puitTaimestik: 0,
            margalaA: 0,
            margalaKa: 0,
            hoone: 0,
            muuRajatis: 0,
            tee: 0,
            muu: 0
        },
        roadType: null,
        plotNumber: "",
        loading: false,
        coords: {"x":null, "y":null} // click coords
    },
    mutations: {
        changeCoords(state, {x, y}){
            state.coords["x"] = x
            state.coords["y"] = y
        },
        setLoading(state, value){
            state.loading = value
        },
        changePlotNumber(state,value){
            state.plotNumber = value
        },
        changeAreas(state, {area, value}){
            state.areas[area] = value
        },
        changeOtherArea(state) {
            state.areas.muu = state.areas.totalSurface
            Object.entries(state.areas).forEach(array => {
                if (array[0] !== 'muu' && array[0] !== 'totalSurface') {
                    state.areas.muu -= array[1]
                }
            })
            if (state.areas.muu < 0) {
                state.areas.muu = 0
            }
        },
        setRoadType(state, roadType) {
            state.roadType = roadType
        }
    },
    getters: {
        getPlotNumber: state => {
            return state.plotNumber
        },
        getLoading: state => {
            return state.loading
        },
        getCoords: state => {
            return state.coords
        },
        getAreas: state => {
            return state.areas
        },
        getRoadType: state => {
            return state.roadType
        }
    }
});