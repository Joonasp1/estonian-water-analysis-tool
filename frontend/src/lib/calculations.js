
export function calculateFlow(flowMult, a,b,c, area, rainFreq, rainDuration){
    // i = (A*p^b)/(t^c)
    let rain_intensity = (a*Math.pow(Number(rainFreq), b))/(Math.pow(Number(rainDuration), c))
    console.log("i=",rain_intensity, "mm/h")
    // convert to l/(s*ha)
    let conv_const = 166.7 // l/(s*ha)
    rain_intensity = rain_intensity * (1/60) * conv_const

    // convert area from m2 to ha
    area = area * 0.0001

    // Q = C * i * A
    return Number(flowMult) * rain_intensity * Number(area)

}

export function calculateRainFrequencyPeriod(intensity, duration, a, b, c){
    // console.log("Intensity ", intensity)
    // console.log("Duration ", duration)
    // console.log("a ", a)
    // console.log("b ", b)
    // console.log("c ", c)

    let prob = Math.pow(((intensity*Math.pow(duration,c))/a),(1/b))
    // console.log(prob)
    return prob

}