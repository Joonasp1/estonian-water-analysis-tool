
import axios from 'axios'
import store from "../store/index"

// fetches the areas from the backend and saves to the vue store
export function getAreasByLandId(id){

    try{
        store.commit("setLoading", true);
        axios.get("/api/land/area/" + String(id)).then((response) => response.data).then(data =>{
          if (data['status'] === 'fail') {
            alert(data['error'])
          }
          
          // set plotNr for search box
          store.commit("changePlotNumber", data["tunnus"])
    
          let hoonedala = 0
          let gardenArea = 0
          let hooned = data["E_401_hoone"]["objektid"]
          hooned.forEach(hoone => {
            hoonedala = hoonedala + hoone["pindala"]
          });
    
          let gardens = data["E_302_ou_a"]["objektid"]
          let gardenCovers = data["E_302_ou_a"]["katvad_objektid"]
          gardens.forEach(garden => {
            gardenArea += garden["pindala"]
            if (gardenCovers) {
              gardenCovers.forEach(gardenCover => {
                if (gardenCover["katetav_objekt"] === garden["etak_id"]) {
                  gardenArea -= gardenCover["pindala"]
                }
              })
            }
          })
          let seisuVeekoguAla = 0;
          let seisuVeekogud = data["E_202_seisuveekogu"]["objektid"]
          seisuVeekogud.forEach(veekogu => {
            seisuVeekoguAla = seisuVeekoguAla + veekogu["pindala"]
          })
          let vooluVeekoguAla = 0;
          let vooluVeekogud = data["E_203_vooluveekogu"]["objektid"]
          vooluVeekogud.forEach(veekogu => {
            vooluVeekoguAla = vooluVeekoguAla + veekogu["pindala"]
          })

          let muuKolvikAla = 0;
          let muuKolvikud = data["E_301_muu_kolvik_a"]["objektid"]
          let muuKolvikudCovers = data["E_301_muu_kolvik_a"]["katvad_objektid"]
          muuKolvikud.forEach(kolvik => {
            muuKolvikAla = muuKolvikAla + kolvik["pindala"]
            if (muuKolvikudCovers){
              muuKolvikudCovers.forEach(kolvikCover => {
                if (kolvikCover["katetav_objekt"] === kolvik["etak_id"]) {
                  muuKolvikAla -= kolvikCover["pindala"]
                }
              })
            }
          })
          let muuKolvikudKA = data["E_301_muu_kolvik_ka"]["objektid"]
          let muuKolvikudKACovers = data["E_301_muu_kolvik_ka"]["katvad_objektid"]
          muuKolvikudKA.forEach(kolvik => {
            muuKolvikAla += kolvik["pindala"]
            if (muuKolvikudKACovers) {
              muuKolvikudKACovers.forEach(kolvikCover => {
                if (kolvikCover["katetav_objekt"] === kolvik["etak_id"]) {
                  muuKolvikAla -= kolvikCover["pindala"]
                }
              })
            }
          })
          

          let haritavMaaAla = 0;
          let haritavMaad = data["E_303_haritav_maa_a"]["objektid"]
          haritavMaad.forEach(maa => {
            haritavMaaAla = haritavMaaAla + maa["pindala"]
          })
          let lageAla = 0;
          let lagedad = data["E_304_lage_a"]["objektid"]
          let lagedadCovers = data["E_304_lage_a"]["katvad_objektid"]
          lagedad.forEach(lage => {
            lageAla = lageAla + lage["pindala"]
            if (lagedadCovers){
              lagedadCovers.forEach(lageCover => {
                if (lageCover["katetav_objekt"] === lage["etak_id"]) {
                  lageAla -= lageCover["pindala"]
                }
              })
            }
          })

          let puitTaimestikAla = 0;
          let puitTaimestikud = data["E_305_puittaimestik_a"]["objektid"]
          let puitTaimestikudCovers = data["E_305_puittaimestik_a"]["katvad_objektid"]
          puitTaimestikud.forEach(taimestik => {
            puitTaimestikAla = puitTaimestikAla + taimestik["pindala"]
            if (puitTaimestikudCovers){
              puitTaimestikudCovers.forEach(puitCover => {
                if (puitCover["katetav_objekt"] === taimestik["etak_id"]) {
                  puitTaimestikAla -= puitCover["pindala"]
                }
              })
              
            }
          })
          let margalaAAla = 0;
          let margaladA = data["E_306_margala_a"]["objektid"]
          margaladA.forEach(ala => {
            margalaAAla = margalaAAla + ala["pindala"]
          })
          let margalaKaAla = 0;
          let margaladKa = data["E_306_margala_ka"]["objektid"]
          margaladKa.forEach(ala => {
            margalaKaAla = margalaKaAla + ala["pindala"]
          })
          let muuRajatisAla = 0;
          let muudRajatised = data["E_403_muu_rajatis"]["objektid"]
          muudRajatised.forEach(rajatis => {
            muuRajatisAla = muuRajatisAla + rajatis["pindala"]
          })

          let teeAla = 0;
          let teed = data["E_501_tee_a"]["objektid"]
          let teedKate = data["E_501_tee_a"]["katvad_objektid"]
          const roadTypes = {};
          teed.forEach(tee => {
            if ('tyyp' in tee) {
              tee['tyyp'] in roadTypes ? roadTypes[tee['tyyp']] += tee["pindala"] : roadTypes[tee['tyyp']] = tee["pindala"]
            }

            teeAla = teeAla + tee["pindala"]
            if (teedKate) {
              teedKate.forEach(teekate => {
                if (teekate["katetav_objekt"] === tee["etak_id"]) {
                  teeAla -= teekate["pindala"]
                }
              })
            }
          })
    
          let totalSurface = Math.round(data["katastri_pindala"])
          
          store.commit('changeAreas', { area: "totalSurface", value: Math.round(totalSurface)})
          store.commit('changeAreas', { area: "roofSurface", value: Math.round(hoonedala)})
          store.commit('changeAreas', { area: "grassSurface", value: Math.round(gardenArea)})
          store.commit('changeAreas', { area: "seisuVeekogu", value: Math.round(seisuVeekoguAla)})
          store.commit('changeAreas', { area: "vooluVeekogu", value: Math.round(vooluVeekoguAla)})
          store.commit('changeAreas', { area: "muuKolvik", value: Math.round(muuKolvikAla)})
          store.commit('changeAreas', { area: "haritavMaa", value: Math.round(haritavMaaAla)})
          store.commit('changeAreas', { area: "lage", value: Math.round(lageAla)})
          store.commit('changeAreas', { area: "puitTaimestik", value: Math.round(puitTaimestikAla)})
          store.commit('changeAreas', { area: "margalaA", value: Math.round(margalaAAla)})
          store.commit('changeAreas', { area: "margalaKa", value: Math.round(margalaKaAla)})
          store.commit('changeAreas', { area: "muuRajatis", value: Math.round(muuRajatisAla)})
          store.commit('changeAreas', { area: "tee", value: Math.round(teeAla)})
          store.commit('changeOtherArea')
          store.commit('setRoadType', Object.keys(roadTypes).length === 0 ? null : Object.keys(roadTypes).reduce((a, b) => roadTypes[a] > roadTypes[b] ? a : b).toLowerCase())
          
        }).catch(err => {
            // error/exception handling
            if (err.response && err.response.status === 404){
              alert("Sellise id'ga katastrit ei leitud! / A land with such id does not exist");
            }else{
              console.log(err)
              alert("Unspecified error - check if data is in correct format")
            }
        }).finally(() => store.commit("setLoading", false))
      }
      catch(err){
        console.log(err);
      }

}

