import VueRouter from 'vue-router'
import RetentionCalc from '../components/RetentionCalc.vue'
import RainfallProb from '../components/RainfallProb.vue'
import WeatherGraphs from '../components/WeatherGraphs.vue'

const routes = [
  {
    path: '/',
    redirect: 'veepidavus'
  },
  {
    path: '/veepidavus',
    component: RetentionCalc
  },
  {
    path: '/sadu',
    component: RainfallProb
  },
  {
    path: '/ilm',
    component: WeatherGraphs
  },
]
const router = new VueRouter({
    mode: 'history',
    routes
})
export default router