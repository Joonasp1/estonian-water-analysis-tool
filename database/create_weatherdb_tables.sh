# make sure the container is running:
# make deploy

docker cp ./create_weather_db_tables.sql weather_db:/tables.sql;
docker exec -u postgres weather_db psql -d EWAT_WEATHER -f /tables.sql