#!/bin/bash
shp2pgsql -D -I -s 3301 -d -t 2D shapefiles/E_301_muu_kolvik_a.shp kolvik_a | psql postgresql://postgres:testpass@localhost:5432/EWAT
shp2pgsql -D -I -s 3301 -d -t 2D -W latin1 shapefiles/E_301_muu_kolvik_ka.shp kolvik_ka | psql postgresql://postgres:testpass@localhost:5432/EWAT
shp2pgsql -D -I -s 3301 -d -t 2D shapefiles/E_302_ou_a.shp ou | psql postgresql://postgres:testpass@localhost:5432/EWAT
shp2pgsql -D -I -s 3301 -d -t 2D shapefiles/E_303_haritav_maa_a.shp haritav_maa | psql postgresql://postgres:testpass@localhost:5432/EWAT
shp2pgsql -D -I -s 3301 -d -t 2D shapefiles/E_304_lage_a.shp lage | psql postgresql://postgres:testpass@localhost:5432/EWAT
shp2pgsql -D -I -s 3301 -d -t 2D shapefiles/E_305_puittaimestik_a.shp puittaimestik | psql postgresql://postgres:testpass@localhost:5432/EWAT
shp2pgsql -D -I -s 3301 -d -t 2D shapefiles/E_306_margala_a.shp margala_a | psql postgresql://postgres:testpass@localhost:5432/EWAT
shp2pgsql -D -I -s 3301 -d -t 2D shapefiles/E_306_margala_ka.shp margala_ka | psql postgresql://postgres:testpass@localhost:5432/EWAT