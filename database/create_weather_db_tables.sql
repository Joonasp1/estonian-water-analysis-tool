-- To run this file in the weather_db container use
-- create_weatherdb_tables.sh

CREATE TABLE IF NOT EXISTS stations(
    station_id INTEGER PRIMARY KEY,
    station_name VARCHAR(20) UNIQUE,
    product_nr VARCHAR(20),
    active BOOLEAN,
    longitude REAL NOT NULL,
    latitude REAL NOT NULL,
    elevation REAL,
    timezone VARCHAR(50),
    city VARCHAR(20),
    entry_timestamp TIMESTAMP default current_timestamp
);

CREATE TABLE IF NOT EXISTS weather_data(
    pk SERIAL PRIMARY KEY,
    station_id INTEGER,
    datadesc VARCHAR(50) NOT NULL,
    content TEXT NULL,
    creation_timestamp TIMESTAMP default current_timestamp,

    CONSTRAINT fk_station_id
        FOREIGN KEY(station_id)
            REFERENCES stations(station_id)
    
);