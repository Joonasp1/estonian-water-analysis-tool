from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import REAL, TEXT, VARCHAR, Boolean, Integer
import logging


log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

engine = create_engine('postgresql://postgres:loggerdbpass@weather_db:5432/EWAT_WEATHER')
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
# Base.query = db_session.query_property()


class Stations(Base):
    __tablename__ = "stations"
    station_id = Column(Integer, primary_key=True)
    station_name = Column(VARCHAR(20))
    product_nr = Column(VARCHAR(20))
    active = Column(Boolean)
    longitude = Column(REAL, nullable=False)
    latitude = Column(REAL, nullable=False)
    elevation = Column(REAL)
    timezone = Column(VARCHAR(50))
    city = Column(VARCHAR(20))

class Weather(Base):
    __tablename__ = "weather_data"
    pk = Column(Integer, primary_key=True)
    station_id = Column(Integer)
    datadesc = Column(VARCHAR(50), nullable=False)
    content = Column(TEXT)

# deletes the old stations data and inserts the fresh entries
def update_stations_data(data):
    log.debug("Updating stations data in the database")
    try:
    
        for station in data:
            station_id = int(station["station_id"])
            station_name = station["station_name"]
            product_nr = station["product_number"]
            active = station["active"]
            longitude = float(station["longitude"])
            latitude = float(station["latitude"])
            elevation = float(station["elevation"])
            timezone = station["time_zone"]
            city = station["city"]
            
            # if a station with such id exists - update
            existing_entry = db_session.query(Stations).filter(Stations.station_id == station_id).first()
            if (existing_entry):
                log.debug("Updating existing station entry")
                db_session.query(Stations).filter(Stations.station_id == station_id).update({
                    Stations.station_name: station_name,
                    Stations.product_nr: product_nr,
                    Stations.active: active,
                    Stations.longitude: longitude,
                    Stations.latitude: latitude,
                    Stations.elevation: elevation,
                    Stations.timezone: timezone,
                    Stations.city: city
                })
            else:
                db_session.add(Stations(station_id=station_id, station_name=station_name,
                    product_nr=product_nr, active=active, longitude=longitude, latitude=latitude,
                    elevation=elevation, timezone=timezone, city=city))
            

    except Exception as e:
        log.error("Unable to update stations info in the database. " + str(e))
        db_session.rollback()
        return
    db_session.commit()

# extracts the entry with tx_id = 1, if such an entry with such key is not present returns the first occurence of a data instance
# might need adjustment when new weather stations are added
def extractCorrectData(data):
    if len(data) == 0: return {}
    result = data[0]["data"][0]
    
    for data_instance in data:
        if "tx_id" in data_instance["data"][0].keys() and data_instance["data"][0]["tx_id"] == 1:
            result = data_instance["data"][0]
            break
    
    # dict with key:value pairs of the weather data
    return result

# logs data from the weather station to the database
def log_weather_entry(station_id, data, params_to_be_logged):
    log.debug("Adding new weather data to the database")

    info = extractCorrectData(data)

    try:
        new_rows = []
        for param in params_to_be_logged:
            if info[param] != None:
                new_rows.append(Weather(station_id=station_id, datadesc=str(param), content=str(info[param])))
            else:
                new_rows.append(Weather(station_id=station_id, datadesc=str(param), content=None))

        db_session.add_all(new_rows)

    except Exception as e:
        log.error("Unable to log the weather entry to the database! ", e)
        db_session.rollback()
        return
    db_session.commit()
    log.debug("Successfully added new weather data to the database.")
    
# returns the names of the stations available in the database
def get_stations_name_and_id():
    try:
        stat = []
        for row in db_session.query(Stations.station_id, Stations.station_name):
            stat.append((row[0], row[1]))
        return stat
    except Exception as e:
        db_session.rollback()
        log.error("Unable to fetch current stations from the database! " + str(e))
        raise e

def flush_db():
    pass

