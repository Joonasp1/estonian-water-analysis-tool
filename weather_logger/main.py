import schedule
import time
import logging
import requests
import os
import db

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

log_interval = int(os.environ["LOGGING_INTERVAL_MIN"])
db_backup_interval = int(os.environ["DATABASE_BACKUP_INTERVAL_DAYS"])
db_flush_interval = int(os.environ["DATABASE_FLUSH_DAYS"])
station_update_interval = int(os.environ["STATION_UPDATE_HOURS"])


logParams = {
    "Valga_keskjaam": ["temp","hum","wind_speed_avg_last_10_min","rainfall_last_15_min_mm", "rainfall_last_24_hr_mm","wind_dir_scalar_avg_last_10_min"],
    "Aardla_95": ["temp_out", "hum_out", "bar", "wind_speed_10_min_avg", "rain_day_mm", "wind_dir"]
    # add loggable stations and params here
    # other data is updated automatically
}

# keeps available stations data up to date in the database
def update_station_info():
    log.debug("Updating sensor info")
    try:
        data = requests.get("http://gateway/api/weather/stations").json()
        log.debug(data)
        db.update_stations_data(data["stations"])

    except Exception as e:
        log.error("Unable to fetch new sensor data! " + str(e))



def log_weather():
    log.debug("Logging weather" + str(time.localtime()))
    try:
        stations_id_name = db.get_stations_name_and_id()
        # for all stations
        for id, name in stations_id_name:
            data = requests.get("http://gateway/api/weather/current", params={"station_name": name}).json()
            # log only stations that are specified in the logParams dict above
            if name in logParams.keys():
                db.log_weather_entry(id, data["sensors"], logParams[name])

    except Exception as e:
        log.error("Unable to log weather data! " + str(e))

    

# do once on startup
update_station_info()
log_weather()


schedule.every(log_interval).minutes.do(log_weather)
schedule.every(station_update_interval).hours.do(update_station_info)
# todo: add other events


while True:
    schedule.run_pending()
    time.sleep(1)